alias ll='ls -lvX --almost-all --group-directories-first --human-readable'
alias whois='whois -H'
alias ip='ip --color=auto'
