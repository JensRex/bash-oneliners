#!/bin/bash
# Find out what's being served in the company canteen.
# depends: bash curl html-xml-utils imagemagick tesseract-ocr tesseract-ocr-dan
# Jens Rex <jrex@blue-ocean-robotics.com>
# 2023-07-03
declare -a dag=("MANDAG" "TIRSDAG" "ONSDAG" "TORSDAG" "FREDAG") && curl -s $(curl --compressed -s https://www.xn--kokkenslillekkken-d1b.dk/ugens-menu | hxclean | hxselect img::attr\('src'\) -c -s '\n' | grep Menu) | mogrify -threshold 35% -write - - | tesseract -l dan --psm 11 --dpi 600 - - | grep -Pzo ${dag[$(date +%u)-1]}+'[\r\n]+([^\r\n]+)' && echo && unset dag

# create array of week days
# get the source of the canteen menu page, look for an image with "Menu" in the filename
# filter it to b/w
# ocr
# get weekday-number array index minus one, because you can't assume user locale == daDK
# regex output for weekday name plus the next non-blank line
